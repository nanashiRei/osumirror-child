<?php include 'inc/api_config.php'; ?>
<!DOCTYPE html>
<html>
<head>
<title><?php echo MIRROR_TITLE; ?></title>
</head>
<body>
<h2><?php echo MIRROR_TITLE; ?></h2>
<p><img src="/mirror_logo.png" alt="mirror logo" /></p>
<pre>
<?php

echo 'Running [OK]', chr(10);

$maps = glob(OSUFILES_PATH.'/maps/*');
$size_maps = 0;
foreach($maps as $map) $size_maps += filesize($map);

echo 'Maps [', count($maps), '] ';
printf('%0.3f MiB', $size_maps / pow(1024,2));
echo chr(10);

$packs = glob(OSUFILES_PATH.'/packs/*/*.rar');
$size_packs = 0;
foreach($packs as $pack) $size_packs += filesize($map);

echo 'Packs [', count($packs), '] ';
printf('%0.3f MiB', $size_packs / pow(1024,2));
echo chr(10);

?>
</pre>
</body>
</html>