<?php

/*
 * You need to configure this 
 * */
// Must be exactly 32 digits long, only alphanumeric (a-z, A-Z, 0-9)
define('ENCRYPTION_SECRET', '00001111000011110000111100001111');
// Will be displayed on the index
define('MIRROR_TITLE', 'Your Mirror Title');
// Use direct if you are unsure. But beware, this will remove all limitations.
define('MIRROR_TYPE', 'apache2'); // 'apache2', 'nginx' or 'direct'

/*
 * This should be correct in 99% of all cases
 * You shouldn't need to change this.
 * */
// Path to where "packs" and "maps" are. Should be the home of the user "osufiles".
define('OSUFILES_PATH', '/home/osufiles');
// Don't touch this! If you change it your mirror will not work at all.
define('ENCRYPTION_KEY', 'osumiroR');